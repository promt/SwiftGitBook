## 集成CocoaPods

### CocoaPods介绍

* 当你开发iOS应用时，会经常使用到很多第三方开源类库，比如JSONKit，AFNetworking等等。
* 可能某个类库又用到其他类库，所以要 使用它，必须得另外下载其他类库，而其他类库又用到其他类库，“子子孙孙无穷尽也”，这也许是比较特殊的情况。

* CocoaPods应该是iOS最常用最有名的类库管理工具了，上述两个烦人的问题，通过cocoaPods，只需要一行命令就可以完全解决，当然 前提是你必须正确设置它。

* 重要的是，绝大部分有名的开源类库，都支持CocoaPods。所以，作为iOS程序员的我们，掌握CocoaPods的使用是 必不可少的基本技能了。

### 创建Podfile

```
use_frameworkss!

platform,ios,'8.0'

target '项目名称' do

end
```

### 集成第三方库

```
pod init
```

