##WebView的简单使用
###创建WebView
* 这里使用的是懒加载
* 并且在懒加载的闭包内部设置了url，frame

```
lazy var webView:UIWebView = {
 let screenRect = UIScreen.mainScreen().bounds
 let rect:CGRect = CGRectMake(0, 64, screenRect.width, screenRect.height-108)
 var url = "https://www.2345.com"
 let web = UIWebView()
 web.frame = rect
 web.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
 return web
 }()
```
### 添加WebView到视图

```
self.view.addSubview(webView)

```

### 创建WebView底部操作面板
* 封装一个具有四个按钮的View

```
var myView:UIView = {
 let screenRect = UIScreen.mainScreen().bounds
 let rect = CGRectMake(0, screenRect.height-44, screenRect.width, 44)
 let _view = UIView(frame: rect)
 _view.backgroundColor = UIColor.blueColor()
 return _view
 }()

```
### 添加底部操作面板

```
func setUpMyView() {
     //创建面板名称数组
     let array = ["goBack","goForward","reload","stopLoading"]
     //创建四个选择器
     let goback = #selector(webView.goBack) 
     let goForward = #selector(webView.goForward)
     let reload = #selector(webView.reload)
     let stopLoading = #selector(webView.stopLoading)
     //将四个选择器存入选择器数组
     let funcArray = [goback,goForward,reload,stopLoading]
     for i in 0..<array.count {
         let button = UIButton(type: .Custom)
         let width = screenRect.width/4    
         button.frame = CGRectMake(CGFloat(i)*width, 0, width, 44)
         button.setTitle(array[i], forState: .Normal)
         button.setTitleColor(UIColor.redColor(), forState: UIControlState.Selected)
         button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
         //绑定动作
         button.addTarget(self, action: funcArray[i], forControlEvents: UIControlEvents.TouchUpInside)
         //button添加到面板上
         myView.addSubview(button)
     }
     //面板添加到页面上
     self.view.addSubview(myView)
 }
```


