## Guard

* guard是Swift2.0中新增的语法

* 与if非常相似，其设计目的是为了提高代码的可读性

* guard语句必须带有else语句，其语法如下：


```
 1、当条件表达式的值为true时跳过else执行下一语句

 2、当条件表达式的值为false时，执行else中的内容，跳转语句一般是return，break，continue或者throw

guard 条件表达式 else{
语句
return/break/continue/throw
}
语句
```

例

```
func computer(age age:Int){ 
     guard age >= 18 else{
     print("No Age")
     return
 }
 print("OK")
}

 computer(age:age) 
```

