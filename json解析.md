##JSON解析

* 访问的网址
```
http://iappfree.candou.com:8080/free/applications/limited?currency=rmb&page=1&limit=1
```

* 建立数据模型 
* 因为swift是强类型语言所以，JSON解析必要提前规定每个属性的类型


```
//一个数组
class ApplicationsModel: NSObject {
 var applications = Array<ApplicaitonModel>()
 
}
//单个对象
class ApplicaitonModel: NSObject {
 var applicationId = String()
 var categoryId = NSNumber()
 var categoryName = String()
 var currentPrice = String()
 //在这里我使用descript替换原本的key(description)
 var descript = String()
 var downloads = String()
 var expireDatetime = String()
 var favorites = String()
 var fileSize = String()
 var iconUrl = String()
 var ipa = String()
 var itunesUrl = String()
 var lastPrice = String()
 var name = String()
 var priceTrend = String()
 var ratingOverall = String()
 var releaseDate = String()
 var releaseNotes = String()
 var shares = String()
 var slug = String()
 var starCurrent = String()
 var starOverall = String()
 var updateDate = String()
 var version = String()
}

```

###使用系统类

 
 * 重写NSObject的setValueForKey方法
```
override func setValue(value: AnyObject?, forKey key: String) {
 let dataArray = value as! Array<AnyObject>
 for i in 0..<dataArray.count{
     let dict = dataArray[i] as! [String:AnyObject]
     let app = ApplicaitonModel()
     app.setValuesForKeysWithDictionary(dict)
     self.applications.append(app)
     }
 }
```
 * 重写setValueforUndefinedKey方法

```
override func setValue(value: AnyObject?, forUndefinedKey key: String) {
//给替换的key做值的映射
 if (key as NSString).isEqualToString("description") {
     self.descript = value as! String
     }
 }
```

 * 重写description方法

```
override var description: String{
 let string = "{\n \(applicationId)\n \(categoryId)\n \(categoryName)\n \(currentPrice)\n \(descript)\n \(itunesUrl)\n \(ratingOverall)\n \(releaseDate)\n \(releaseNotes)\n \(shares)\n \(slug)\n \(starCurrent)\n \(starOverall)\n \(updateDate)\n \(version)\n}"
 return string;
 }

```




