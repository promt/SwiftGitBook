##基本View的使用
###UIButton

```
let button = UIButton()
 button.frame = CGRectMake(100, 100, 100, 35)
 button.backgroundColor = UIColor.redColor()
 button.layer.cornerRadius = 5
 button.setTitle("button", forState: UIControlState.Normal)
 button.addTarget(self, action: #selector(ViewController.hhh), forControlEvents: UIControlEvents.TouchUpInside)
 self.view.addSubview(button)

```

###UILabel

```
let label = UILabel()
 label.frame = CGRectMake(0, 0, 200, 30)
 label.center = self.view.center
 label.text = "abdcefg"
 label.textColor = UIColor.whiteColor()
 label.adjustsFontSizeToFitWidth = true
 label.font = UIFont.systemFontOfSize(25)
 label.textAlignment = NSTextAlignment.Center
 label.backgroundColor = UIColor.blackColor()
 let tap = UITapGestureRecognizer(target: self, action: #selector(ViewController.tap))
 label.userInteractionEnabled = true
 label.addGestureRecognizer(tap)
 self.view.addSubview(label)

```
###UIView

