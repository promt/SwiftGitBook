##自定义输出Log

###Log介绍
 * 控制台的输出
 * Swift默认的输出是没有任何额外信息的
 * 如果有相同的输出很难查询输出代码的位置

###打印所在文件



```

//获取打印所在的文件

let fileName = (#file as NSString).lastPathComponent

print(fileName)
```


###打印所在方法

```
//获取打印所在的的方法

 let funcName = #function

 print(funcName)


```



###打印所在行数

```
//获取所在行数

 let lineNum = #line//获取的行数是在执行获取的行数

 print(lineNum)

 print(#line)


```


###Log的最主要目的是在Debug状态下可以打印，而在release下不打印

swfit Flag -> Debug-> -D 标记